*If you are planning on playing multiplayer, make sure you register at [account.veloren.net](https://account.veloren.net/) before connecting to an auth-enabled server. Otherwise, things remain mostly the same for both singleplayer and multiplayer. Keep in mind that every aspect of the game is subject to change, and this guide may not be 100% up to date when you read it.*


# Character Creation

![character creation](character_creation.jpg)

Welcome to the character creation screen! Currently only aesthetic choices are shown; eventually this interface will be expanded to contain lore on each species, guidance on chosen weapons, and starting stats of each species. As of now, however, your choice of species matters little.

There are currently six playable species in Veloren; Human, Orc, Dwarf, Elf, Undead, and Danari. The Danari are the only species you won’t be familiar with; they’re a special species created for the game.

As well as choosing your species, here you choose your starting weapon. There are no species restrictions on weapons, and choosing your starter weapon does not lock you into that choice; as you play you’ll find weapons of the other types and can switch to those with no penalty.

You can set your hair style and colour, skin colour, eye details, eye colour, and - only available to some species - accessories. Use the randomize button (the dice icon) to randomize your character's appearance and name.

When you’ve finished creating your character, enter a name, and then click Create. Choose your character from the list, click Enter World, and you’ll enter the game world.


# Getting Started

![getting started](getting_started.jpg)

You’ll enter the world in a little village nestled at the foot of a mountain. This is the default world and will be the same for every character you make. This is a pre-generated world; it is possible to generate your own world with a new seed, but this process takes a long time, and so this current world has been pre-generated for your convenience.

Look around your interface. At the top left you’ll see that you can press F1 to show keybinds; have a look at this. On the bottom right, you’ll see a few icons and their corresponding keybinds.

- `P` - Opens spells (not currently implemented)
- `M` - Opens the map (this isn’t the final map; the map system is being reworked)
- `O` - Opens your social tab
- `N` - Opens your settings
- `B` - Opens your bag / inventory
- `C` - Opens the crafting menu

You can change most key bindings in the settings.

Movement follows the conventions of other similar games, but you will want to know how to pull out your glider (`Shift`) and how to climb vertical surfaces (hold `Space`).


# Inventory and Items

Press `B` to open your inventory.

Items in your inventory can be used by right-clicking them, which usually either equips it on your character, swapping with whatever gear was in its slot, or consumes the item, applying its effects.

Inventory slots can be assigned to hotbar slots by clicking and dragging the item stored in the slot to the hotbar. Any items in that inventory slot will then appear in the assigned hotbar slot. Hotbar slots can be reassigned by dragging an item in a different inventory slot to it. Using an item from a hotbar slot has the same effect as right-clicking it from the inventory.

![managing your inventory](managing_your_inventory.jpg)

Here, `7` will use the Dwarven cheese, but if I run out of cheese and another item takes its slot in the inventory, for example, `7` will use that item instead.

*Depending on your equipped weapon, some hotbar slots will be reserved for abilities.*

If your inventory is getting cluttered or full, you can rearrange the items by dragging them around, or throw them away by dragging them outside the inventory window. Items you throw away can be picked back up as long as you stay near them.

There are many items to collect, and they come in the form of tools, crafting materials, gear, and consumables.

Gear items are items that can be equipped on your character. They can be crafted from materials or looted from enemies and chests and come in several rarities. Gear is one of the main ways to progress your character.

Consumables are items that are, well, consumed on use. They come in several varieties, the most prominent of which are food and potions. Food affects you at a certain rate for its duration while potions take effect instantaneously.

![a mushroom](mushroom.jpg)

If you look carefully you may notice certain objects in the world will highlight when you move your crosshair near them. This indicates you can interact with them (`E` by default), which will usually give you an item.

# Crafting

![the crafting menu](crafting_menu.jpg)

You can open the crafting menu with the `C` key. You'll see a list of craftable items, some of which may be greyed out - that means you have insufficient materials or tools to craft them. Items can be crafted with materials either gathered from the world or dropped by enemies. Food items can be crafted into even more effective foods, and some basic weapons and armor can also be crafted.

*The fireworks are for celebratory use only. Ignite responsibly.*


# Weapons And Combat

![a challenging encounter](combat.jpg)

As you wander the world of Veloren, you'll encounter all manner of animals, monsters, NPCs, and maybe even players. In general, predatory animals and monsters will attack you on sight, while docile creatures and NPCs will ignore you unless attacked.

Each weapon has a left-click basic attack and one or more abilites on right click and the hotbar. Each weapon has its own playstyle, and serve to replace conventional 'classes.' 

Using more than one weapon by equipping it to the 'offhand' will increase your combat options immensely! Switch to your offhand weapon by pressing `Tab`.

Dodging enemy attacks is important. Middle-click will swiftly roll you out of the way. Use sideways movement to avoid ranged attacks while closing in or returning fire. If you use ranged weapons against a melee attacker, try to keep your distance. 

Use a training dummy (found in towns) to familiarize yourself with your weapons' attacks and abilities.

![character progression](character_progression.jpg)

Defeating enemies earns you experience, which allows you to level up your character.

Getting levels allows you to get skill points to spend in a skill tree.
There is currently one skill tree for each weapon, and a general combat skill tree.

![general combat skill tree](SP_general_combat.jpg)

The general combat gives access to some general skills, as well as unlocking weapon-specific skill trees.

![bow skill tree](SP_bow.jpg)

When you earn X experience points, they go into the following skill tree experience bar:

- X for general combat
- X/2 for main weapon, if you've unlocked the corresponding skill tree
- X/2 for secondary weapon, if you've unlocked the corresponding skill tree

So, if you get 4 XP when killing something with the bow equipped as main weapon, the sword equipped at secondary, both unlocked,
you'll get 4 XP in general combat, 2 in bow, 2 in sword.

If you have two bows equipped, you'll have 4 XP in the bow skill tree.
If you have one bow as main weapon and nothing as secondary, you'll also get 4 XP in the bow skill tree.


# Taming

In the course of playing the game, you'll be able to craft Collars, which let you tame creatures. The current maximum number of creatures you can have tamed at once is three. You can only tame non-hostile creatures. Choose carefully!

Tamed creatures will follow you around and will attack anything that attacks you. They can be healed with a healing sceptre. 

Due to pathfinding issues, pets may have difficulty keeping up with you when you go off gliding or delve into dungeons.

*Currently, pets do not persist through logins. Don't get too attached.*


# Map

![map](map_icons.jpg)

Access the map by pressing `M`. You'll see icons representing dungeons, towns, and cave entrances along with brown lines denoting roads. The difficulty level of a dungeon is represented by the number of dots that appear above the icon. Your current location is initially at the center of the screen, but the map can be shifted around by clicking and dragging. The mouse wheel zooms in and out. There is also a minimap seen in regular play at the top right. 


# Campfires

![campfire](campfire.jpg)

Campfires are interspersed across the world, marked by tall pillars of smoke. Approaching a campfire will show the message ‘Waypoint Saved’. When you die, you will respawn at the last campfire that saved your waypoint. Your waypoint persists through logins, so you can start each session approximately where you left off!

Sitting near a campfire (with the `K` key) will heal you over time. If you ever need respite from your travels, campfires are what you should look for!


# Gliding

![glider](glider.jpg)

Getting a hang on gliders may cost you a few broken bones, but once you get a feel for it you may find yourself looking for any excuse to explore a high place.

## Pre-flight

Pressing the `Shift` key will equip the glider. You can run around with it equipped, but you won’t be able to roll or draw your weapon at the same time.

You're going to need a steep slope or a cliff with enough vertical margin that you have time to redirect your fall into a glide. If you need to jump for some extra initial elevation it is recommended that you deploy the glider mid-jump, or else it will push against you on the way up. Another trick is to roll-jump for an extra initial boost.

## Take-off

Once airborn, you probably want to let go of any movement keys, as you'll mainly control the glider by looking in the direction you want to orient it towards. The controls used for movement on the ground are used for quickly adjusting pitch and direction while gliding.

Gliding in Veloren uses a simulated model of aerodynamics; the only acceleration you get is from falling due to gravity, and your only control mechanism is angling the glider against the wind in a way such that the produced lift and drag pushes you in the desired direction. These forces scale with the square of airflow velocity relative to you, so gaining and maintaining speed is important.

## Angle of attack

The angle of attack is your pitch relative to the direction of air flow. It's zero when the glider is perfectly aligned with the wind, positive when pitched up and negative when pitched down. By default (when there is no control input) the glider is slightly pitched up relative to the direction you're looking to make you glide most efficiently (in terms of distance) while looking straight into the wind. 

You get the best performance out of small angles of attack, so pitch and swoop with smooth, minute movements. Pitching too much will cause you to stall; an effect which causes the lift produced to drop sharply, putting you at risk of losing too much speed due to drag with little ability to regain control.

## Hints

- A controlled stall can be used to great effect for braking, but if you overdo it you may find yourself gliding backwards (or not at all).
- Look out for cliffs or steep inclines and pitch down to swoop into a glide.
- Lift scales linearly with angle of attack (for small angles), but the ("induced") drag scales _exponentially_. Balancing lift and drag is key to controlling your velocity, elevation and ultimately gliding distance.


# Dungeons

Dungeons are marked on the map with a 'ruins' icon, along with a difficulty level. Dungeons with difficulties 0-3 are doable by a solo player; above that, you may want to [find a group](https://book.veloren.net/players/getting-started/index.html#grouping-up).

On entering the dungeon you’ll descend a long, spiral staircase that will eventually exit at the first dungeon level, deep below ground. Each level has a series of corridors and rooms containing hostile enemies and treasure chests. The most rare and powerful loot in the game can be obtained from dungeons!

![dungeon internals](dungeon2.jpg)

*Dungeons are dark - equip your lantern by pressing the `G` key!*

Make sure you’re well stocked up on healing items and have some levels under your belt before attempting your first dungeon!


# Caves

Caves are marked with cave entrance icons on the map.

![cave mouth](cave_entrance.jpg)

Several resources can be found in caves, some of which are exclusive to them. Are you prepared to plumb the depths, adventurer?


# Grouping Up

When playing in a multiplayer server, you may wish to team up with other players in order to conquer content without losing track of or hurting each other. The grouping function is your friend here. To invite other players to your group, press `O` to open the player list, click on the character you want to invite, and press Invite. Hovering over the character names will show you the player's account name.

![player menu and grouping up](invite.jpg)


# Additional Hints For Beginners

Houses can contain useful items and equipment for you to pick up!

You might want to practice combat with small animals first; attacking denizens will get you killed quickly. Consider instead what help they may offer you in a time of need!

Once you have enough leather scraps you can craft yourself some initial armor. Look for Apples or Mushrooms and Twigs (common in moderate forests). Combine them in the crafting menu to get Apple/Mushroom Sticks which give superior healing.

Nighttime can make it easier to spot campfires. If you're alone in the night, light sources are likely indications of safety.

You may have noticed that you're able to crouch with `Ctrl`. This is more than just an animation! If a threat is in your path, consider sneaking around it. Once you draw your weapon, though, the jig's up.


# Useful Commands

`/home` - Teleports you to the default spawn and resets your waypoint. This is your best option if the map has changed and you enter the game stuck in solid ground.

`/w` - global chat

`/r` - regional chat

`/g` - group chat

`/t` - direct chat

**Admin Only**

`/give_item [filepath] [amount]` - Gives you items. Use Tab to autocomplete.

`/tp [player]` - Teleports you to a player.

`/spawn [argument 1] [argument 2]` - Press Tab to cycle through options. Allows you to spawn entities.

`/goto [x,y,z]` - Teleports you to the specified coordinates.

`/debug` - Gives you some special equipment ;)
